# Authorization Library

Authorization library is a library that provides a Spring Security (4.x.x) process
in order to authorize the endpoints of a service base on OpenAIRE Authorities.
It can be used with two different session strategies, a stateless and
a Redis http session.

## Stateless

In stateless strategy, there is not a session. A filter makes a request
to an "userinfo" endpoint and creates an Authentication base on the response.
The advantage of this method is that it doesn't need any storage to store 
user's session, but with a cost of an extra http request per request.

### Usage

#### pom.xml

    <dependency>
        <groupId>eu.dnetlib</groupId>
        <artifactId>uoa-authorization-library</artifactId>
        <version>2.1.2</version>
    </dependency>

#### Spring Application/Configuration

    import eu.dnetlib.uoaauthorizationlibrary.configuration.AuthorizationConfiguration;    

    @PropertySources({@PropertySource("classpath:authorization.properties")})
    @Import(AuthorizationConfiguration.class)
    public class Application {
        public static void main(String[] args) {
            SpringApplication.run(Application.class, args);
        }
    }

#### Configuration

    authorization.security.userInfoUrl = http://<domain>/login-service/userInfo # Required, default ""
    authorization.security.session=<session-cookie-name> # Default openAIRESession

## Redis

In Redis strategy, session is stored to a Redis database when a user
authenticates himself through a login service. The disadvantage of 
this strategy is that it needs access to the Redis database
where session is stored.

### Usage

#### pom.xml

    <dependency>
        <groupId>eu.dnetlib</groupId>
        <artifactId>uoa-authorization-library</artifactId>
        <version>2.1.2</version>
        <classifier>redis</classifier>
    </dependency>

#### Spring Application/Configuration

    import eu.dnetlib.uoaauthorizationlibrary.configuration.AuthorizationConfiguration;    

    @PropertySources({@PropertySource("classpath:authorization.properties")})
    @Import(AuthorizationConfiguration.class)
    public class Application {
        public static void main(String[] args) {
            SpringApplication.run(Application.class, args);
        }
    }

#### Configuration

    authorization.secuirty.redis.host=<redis-ip> # Default localhost
    authorization.secuirty.redis.port=<redis-port> # Default 6379
    authorization.secuirty.redis.password=<redis-password> # Default ""
    authorization.security.domain=<domain-suffix> # e.g openaire.eu
    authorization.security.session=<session-cookie-name> # Default openAIRESession


## Authorize Requests

### Authorization Service

In order to simplify the format of the Authorities, you can use 
this spring component to authorize your endpoints. There is also methods to
get user's information.

    public final String PORTAL_ADMIN = "PORTAL_ADMINISTRATOR";
    public final String ANONYMOUS_USER = "ROLE_ANONYMOUS";
    public final String REGISTERED_USER = "REGISTERED_USER";

    /**
     * Type = FUNDER | COMMUNITY | INSTITUTION | PROJECT
     */
    public String curator(String type) {}

    /**
     * Type = FUNDER | COMMUNITY | INSTITUTION | PROJECT
     * 
     * Id = EE, EGI, etc
     */
    public String manager(String type, String id) { }

    /**
     * Type = FUNDER | COMMUNITY | RI | INSTITUTION | PROJECT
     * 
     * Id = EE, EGI, etc
     */
    public String member(String type, String id)

e.g

    @PreAuthorize("hasAnyAuthority("
        + "@AuthorizationService.PORTAL_ADMIN, "
        + "@AuthorizationService.curator(#type), "
        + "@AuthorizationService.manager(#type, #id)) "
    + ")")
    @RequestMapping(value = "{type}/{id}", method = RequestMethod.GET)
    public Entity getEntity(@PathVariable("type") String type, @PathVariable("id") String id) {}

## Spring Security (5.x.x) - Spring boot (2.x.x)

Because of MitreID dependency, in order to use this library
with redis HttpSession, service has to use spring security (4.x.x).
The only way to use this library in a project with spring security 5.x.x
is the Stateless strategy with the following modification in Application
class:

    import eu.dnetlib.uoaauthorizationlibrary.configuration.AuthorizationConfiguration;    

    @PropertySources({@PropertySource("classpath:authorization.properties")})
    @Import(AuthorizationConfiguration.class)
    public class Application {

        public static void main(String[] args) {
            SpringApplication.run(Application.class, args);
        }

        @Bean
        public WebMvcConfigurer corsConfigurer() {
            return new WebMvcConfigurer() {
                    @Override
                    public void addCorsMappings(CorsRegistry registry) {
                        registry.addMapping("/**")
                                .allowedMethods("GET", "POST", "PUT", "DELETE", "HEAD", "OPTIONS")
                                .allowedOriginPatterns("*")
                                .allowCredentials(true);
                    }
            };
        }   
    }
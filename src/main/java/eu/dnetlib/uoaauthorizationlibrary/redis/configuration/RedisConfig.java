package eu.dnetlib.uoaauthorizationlibrary.redis.configuration;

import eu.dnetlib.uoaauthorizationlibrary.configuration.SecurityConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;

@EnableRedisHttpSession
@Configuration
public class RedisConfig {

    private final SecurityConfig securityConfig;
    private static final Logger logger = LogManager.getLogger(RedisConfig.class);

    @Autowired
    public RedisConfig(SecurityConfig securityConfig) {this.securityConfig = securityConfig;}

    @Bean
    public LettuceConnectionFactory connectionFactory() {
        logger.info(String.format("Redis connection listens to %s:%s ", securityConfig.getRedis().getHost(), securityConfig.getRedis().getPort()));
        LettuceConnectionFactory factory = new LettuceConnectionFactory(securityConfig.getRedis().getHost(), Integer.parseInt(securityConfig.getRedis().getPort()));
        if (securityConfig.getRedis().getPassword() != null) factory.setPassword(securityConfig.getRedis().getPassword());
        return factory;
    }

    @Bean
    public CookieSerializer cookieSerializer() {
        DefaultCookieSerializer serializer = new DefaultCookieSerializer();
        serializer.setCookieName(securityConfig.getSession());
        serializer.setCookiePath("/");
        serializer.setDomainName(securityConfig.getDomain());
        return serializer;
    }
}

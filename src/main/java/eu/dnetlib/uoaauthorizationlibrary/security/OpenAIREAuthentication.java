package eu.dnetlib.uoaauthorizationlibrary.security;

import eu.dnetlib.uoaauthorizationlibrary.stateless.utils.UserInfo;
import org.springframework.security.authentication.AbstractAuthenticationToken;

public class OpenAIREAuthentication extends AbstractAuthenticationToken {
    private final UserInfo user;

    public OpenAIREAuthentication() {
        super(null);
        this.user = null;
        setAuthenticated(false);
    }

    public OpenAIREAuthentication(UserInfo user) {
        super(user.getAuthorities());
        this.user = user;
        setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return getUser().getSub();
    }

    @Override
    public Object getPrincipal() {
        return getUser();
    }

    public UserInfo getUser() {
        return user;
    }
}

package eu.dnetlib.uoaauthorizationlibrary.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Date;

@ConfigurationProperties("authorization.global-vars")
public class GlobalVars {
    public static Date date = new Date();
    private Date buildDate;
    private String version;

    public String getBuildDate() {
        if(buildDate == null) {
            return null;
        }
        return buildDate.toString();
    }

    public void setBuildDate(Date buildDate) {
        this.buildDate = buildDate;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}

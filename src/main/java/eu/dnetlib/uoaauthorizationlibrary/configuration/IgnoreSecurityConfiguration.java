package eu.dnetlib.uoaauthorizationlibrary.configuration;

import eu.dnetlib.uoaauthorizationlibrary.security.AuthorizationService;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(
        basePackageClasses = {AuthorizationService.class}
)
public class IgnoreSecurityConfiguration {
}

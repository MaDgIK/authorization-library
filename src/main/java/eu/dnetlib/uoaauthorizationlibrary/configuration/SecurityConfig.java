package eu.dnetlib.uoaauthorizationlibrary.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("authorization.security")
public class SecurityConfig {

    private Redis redis = new Redis();
    private String userInfoUrl;
    private String domain;
    private String session;

    public Redis getRedis() {
        return redis;
    }

    public void setRedis(Redis redis) {
        this.redis = redis;
    }

    public String getUserInfoUrl() {
        return userInfoUrl;
    }

    public void setUserInfoUrl(String userInfoUrl) {
        this.userInfoUrl = userInfoUrl;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }
}

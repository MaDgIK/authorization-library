package eu.dnetlib.uoaauthorizationlibrary.stateless.utils;

import eu.dnetlib.uoaauthorizationlibrary.configuration.SecurityConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Collections;

@Component
public class AuthorizationUtils {
    private final Logger log = LogManager.getLogger(this.getClass());
    private final SecurityConfig securityConfig;

    @Autowired
    AuthorizationUtils(SecurityConfig securityConfig) {
        this.securityConfig = securityConfig;
    }

    public UserInfo getUserInfo(HttpServletRequest request) {
        String url = securityConfig.getUserInfoUrl();
        RestTemplate restTemplate = new RestTemplate();
        try {
            if(url != null && hasCookie(request)) {
                ResponseEntity<UserInfo> response = restTemplate.exchange(url, HttpMethod.GET, createHeaders(request), UserInfo.class);
                return response.getBody();
            }
            return null;
        } catch (RestClientException e) {
            log.error(url + ": " + e.getMessage());
            return null;
        }
    }

    private boolean hasCookie(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if(cookies != null) {
            return Arrays.stream(cookies).anyMatch(cookie -> cookie.getName().equalsIgnoreCase(this.securityConfig.getSession()));
        }
        return false;
    }

    private HttpEntity<HttpHeaders> createHeaders(HttpServletRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set("Cookie", request.getHeader("Cookie"));
        return new HttpEntity<>(headers);
    }
}

package eu.dnetlib.uoaauthorizationlibrary.stateless.security;

import eu.dnetlib.uoaauthorizationlibrary.security.OpenAIREAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Component
public class AuthorizationFilter implements Filter {

    private final AuthorizationProvider authorizationProvider;

    @Autowired
    AuthorizationFilter(AuthorizationProvider authorizationProvider) {
        this.authorizationProvider = authorizationProvider;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {}

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain) throws IOException, ServletException {
        OpenAIREAuthentication auth = authorizationProvider.getAuthentication((HttpServletRequest) req);
        if(auth.isAuthenticated()) {
            SecurityContextHolder.getContext().setAuthentication(auth);
        }
        filterChain.doFilter(req, res);
    }

    @Override
    public void destroy() {

    }
}

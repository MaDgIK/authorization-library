package eu.dnetlib.uoaauthorizationlibrary.stateless.security;

import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public class AuthorizationFilterConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

    private final AuthorizationFilter filter;

    @Override
    public void init(HttpSecurity http) throws Exception {
        http.csrf().disable();
    }

    public AuthorizationFilterConfigurer(AuthorizationFilter filter) {
        this.filter = filter;
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.addFilterBefore(filter, UsernamePasswordAuthenticationFilter.class);
    }

}

package eu.dnetlib.uoaauthorizationlibrary.stateless.security;

import eu.dnetlib.uoaauthorizationlibrary.security.OpenAIREAuthentication;
import eu.dnetlib.uoaauthorizationlibrary.stateless.utils.AuthorizationUtils;
import eu.dnetlib.uoaauthorizationlibrary.stateless.utils.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class AuthorizationProvider {

    private final AuthorizationUtils utils;

    @Autowired
    AuthorizationProvider(AuthorizationUtils utils) {
        this.utils = utils;
    }

    public OpenAIREAuthentication getAuthentication(HttpServletRequest request) {
            UserInfo user = utils.getUserInfo(request);
            if(user != null) {
                return new OpenAIREAuthentication(user);
            }
            return new OpenAIREAuthentication();
    }
}

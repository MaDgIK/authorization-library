package eu.dnetlib.uoaauthorizationlibrary.controllers;

import eu.dnetlib.uoaauthorizationlibrary.configuration.GlobalVars;
import eu.dnetlib.uoaauthorizationlibrary.configuration.SecurityConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/authorization-library")
public class AuthorizationLibraryCheckDeployController {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private SecurityConfig securityConfig;

    @Autowired
    private GlobalVars globalVars;

    @RequestMapping(value = {"", "/health_check"}, method = RequestMethod.GET)
    public String hello() {
        log.debug("Hello from uoa-authorization-library!");
        return "Hello from uoa-authorization-library!";
    }

    @PreAuthorize("hasAnyAuthority(@AuthorizationService.PORTAL_ADMIN)")
    @RequestMapping(value = "/health_check/advanced", method = RequestMethod.GET)
    public Map<String, String> checkEverything() {
        Map<String, String> response = new HashMap<>();
        response.put("authorization.security.redis.host", securityConfig.getRedis().getHost());
        response.put("authorization.security.userInfoUrl", securityConfig.getUserInfoUrl());
        response.put("authorization.security.session", securityConfig.getSession());
        response.put("authorization.security.domain", securityConfig.getDomain());
        if(GlobalVars.date != null) {
            response.put("Date of deploy", GlobalVars.date.toString());
        }
        if(globalVars.getBuildDate() != null) {
            response.put("Date of build", globalVars.getBuildDate());
        }
        if (globalVars.getVersion() != null) {
            response.put("Version", globalVars.getVersion());
        }
        return response;
    }
}
